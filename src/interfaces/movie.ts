/**
 * Created by osho on 7/18/17.
 */
export interface Movie {
  id: number,
  category_id: number,
  name: string,
  rating: number,
  description: string,
  url: string
}
