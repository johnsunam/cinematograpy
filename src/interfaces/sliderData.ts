/**
 * Created by osho on 7/18/17.
 */
export interface SliderData {
  id: number,
  imgUrl: string,
  movie_id: number
}
