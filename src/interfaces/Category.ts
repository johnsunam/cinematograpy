/**
 * Created by osho on 7/18/17.
 */
export interface Category {
  id: number,
  imgUrl: string,
  name: string
}
