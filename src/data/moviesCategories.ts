/**
 * Created by osho on 7/18/17.
 */
export default [
  {
    id: 1,
    imgUrl: 'assets/img/movies-category/action.jpg',
    name: 'Action'
  },
  {
    id: 2,
    imgUrl: 'assets/img/movies-category/comedy.jpg',
    name: 'Comedy'
  },
  {
    id: 3,
    imgUrl: 'assets/img/movies-category/drama.jpg',
    name: 'Drama'
  },
  {
    id: 4,
    imgUrl: 'assets/img/movies-category/exclusive.jpg',
    name: 'Exclusive'
  },
  {
    id: 5,
    imgUrl: 'assets/img/movies-category/latest.jpg',
    name: 'Latest'
  },
  {
    id: 6,
    imgUrl: 'assets/img/movies-category/love-story.jpg',
    name: 'Love Story'
  }
]
