/**
 * Created by osho on 7/18/17.
 */
export default [
  {
    id: 1,
    imgUrl: 'assets/img/for-slider/slider1.jpeg',
    movie_id: 1
  },
  {
    id: 2,
    imgUrl: 'assets/img/for-slider/slider2.jpeg',
    movie_id: 2
  },
  {
    id: 3,
    imgUrl: 'assets/img/for-slider/slider3.jpeg',
    movie_id: 3
  },
  {
    id: 4,
    imgUrl: 'assets/img/for-slider/slider4.jpeg',
    movie_id: 4
  },
  {
    id: 5,
    imgUrl: 'assets/img/for-slider/slider5.png',
    movie_id: 5
  },

]
