/**
 * Created by osho on 7/18/17.
 */
export default [
  {
    id: 1,
    imgUrl: 'assets/img/short-category/short.jpg',
    name: 'Short'
  },
  {
    id: 2,
    imgUrl: 'assets/img/short-category/documentary.jpg',
    name: 'Documentary'
  }
]
