import { Component } from '@angular/core';
import {AlertController, ModalController, NavController} from 'ionic-angular';
import {SearchPage} from "../search/search";
import sliderData from '../../data/sliderData';
import moviesCategories from '../../data/moviesCategories';
import shortCategories from '../../data/shortCategories';
import {SliderData} from "../../interfaces/sliderData";
import {Category} from "../../interfaces/Category";
import {MoviesListPage} from "../movies-list/movies-list";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  dataForSlider: SliderData[];
  moviesCategories: Category[];
  shortCategories: Category[];
  movieType: string = 'movies';
  constructor(private navCtrl: NavController, private alertCtrl: AlertController, private modalCtrl: ModalController) {
    this.dataForSlider = sliderData;
    this.moviesCategories = moviesCategories;
    this.shortCategories = shortCategories;
  }

  onAboutClick() {
    this.alertCtrl.create({
      title: 'About',
      subTitle: 'Who Are We?',
      message: '<b>Cinemaghar</b><br>Cinemaghar is an app developed to step-up the experience of watching Nepali movies online. Users don\'t ' +
      'have to spend their entire time remembering the names of movies they want to watch in order to search for them online. The app notifies user as ' +
      'and when any new movie is uploaded. We constantly update our content to stay current with Nepali Movie Industry.',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            //console.log('cancelled')
          }
        }
      ]
    }).present();
  }

  onSearchMovie() {
    this.modalCtrl.create(SearchPage).present();
  }

  onShowMoviesList(categoryType: string, category: Category) {
    let params = {
      categoryType: categoryType,
      category: category
    };

    this.navCtrl.push(MoviesListPage,params);
  }

}
