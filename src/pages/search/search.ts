import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  searchedMovie: string;

  constructor(private viewCtrl: ViewController) {}

  dummySearch(event: any) {
    this.searchedMovie = event.target.value;
  }

  onCloseModal() {
    this.viewCtrl.dismiss();
  }

}
