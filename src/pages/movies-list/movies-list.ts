import {Component, OnInit} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Category} from "../../interfaces/Category";

@Component({
  selector: 'page-movies-list',
  templateUrl: 'movies-list.html',
})
export class MoviesListPage implements OnInit{
  categoryType: string;
  category: Category;

  constructor(private navCtrl: NavController, private navParams: NavParams) {

  }

  ngOnInit(): void {
    this.categoryType = this.navParams.get('categoryType');
    this.category = this.navParams.get('category');
  }


}
